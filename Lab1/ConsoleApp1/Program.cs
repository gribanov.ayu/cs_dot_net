﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ConsoleApp1
{
    class VideoGame
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public float Rating { get; set; }
        public Studio Studio { get; set; }
    }

    class Studio
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }

        public int VideoGameID { get; set; }
        public VideoGame VideoGame { get; set; }
        public Publisher Publisher { get; set; }
    }

    class Publisher
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public List<Studio> Studios { get; set; }
        public Location Location { get; set; }
    }

    class Location
    {
        [Key]
        public int ID { get; set; }
        public string Address { get; set; }
        public int PublisherID { get; set; }
        public Publisher Publisher { get; set; }
    }

    class GameContext: DbContext
    {
        public DbSet<VideoGame> VideoGames { get; set; }
        public DbSet<Studio> Studios { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Location> Locations { get; set; }

        public GameContext()
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VideoGame>()
            .HasOne(a => a.Studio)
            .WithOne(a => a.VideoGame)
            .HasForeignKey<Studio>(c => c.VideoGameID)
            .OnDelete(DeleteBehavior.Cascade);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=GamesDb;Trusted_Connection=True;");
        }
    }

    class Program
    {
        static void InitDB()
        {
            using (var GamesBD = new GameContext())
            {
                if(!GamesBD.VideoGames.Any())
                {
                    VideoGame v1 = new VideoGame { Name = "Star Wars: Knights of the Old Republic", Genre = "RPG", Rating = 9.87F };
                    VideoGame v2 = new VideoGame { Name = "Max Payne 2: The Fall of Max Payne", Genre = "Shooter", Rating = 8.52F };
                    VideoGame v3 = new VideoGame { Name = "Grand Theft Auto: Vice City", Genre = "Shooter", Rating = 9.25F };
                    VideoGame v4 = new VideoGame { Name = "Prince of Persia: The Sands of Time", Genre = "RPG", Rating = 8.7F };
                    VideoGame v5 = new VideoGame { Name = "Beyond Good & Evil", Genre = "RPG", Rating = 7.68F };
                    VideoGame v6 = new VideoGame { Name = "Silent Hill 3", Genre = "Horror", Rating = 8.63F };

                    GamesBD.VideoGames.AddRange(v1, v2, v3, v4, v5, v6);
                    GamesBD.SaveChanges();

                    Studio s1 = new Studio { Name = "BioWare", VideoGame = v1 };
                    Studio s2 = new Studio { Name = "Remedy Entertainment", VideoGame = v2 };
                    Studio s3 = new Studio { Name = "Rockstar North", VideoGame = v3 };
                    Studio s4 = new Studio { Name = "Ubisoft Montreal", VideoGame = v4 };
                    Studio s5 = new Studio { Name = "Ubisoft Milan", VideoGame = v5 };
                    Studio s6 = new Studio { Name = "Konami Computer Entertainment Tokyo", VideoGame = v6 };

                    GamesBD.Studios.AddRange(s1, s2, s3, s4, s5, s6);
                    GamesBD.SaveChanges();

                    Publisher p1 = new Publisher { Name = "LucasArts", Studios = new List<Studio>() { s1 } };
                    Publisher p2 = new Publisher { Name = "Rockstar Games", Studios = new List<Studio>() { s2, s3 } };
                    Publisher p3 = new Publisher { Name = "Ubisoft", Studios = new List<Studio>() { s4, s5 } };
                    Publisher p4 = new Publisher { Name = "Konami", Studios = new List<Studio>() { s6 } };

                    GamesBD.Publishers.AddRange(p1, p2, p3, p4);
                    GamesBD.SaveChanges();

                    Location l1 = new Location { Address = "San Francisco, US", Publisher = p1 };
                    Location l2 = new Location { Address = "New York City, US", Publisher = p2 };
                    Location l3 = new Location { Address = "Montreuil, France", Publisher = p3 };
                    Location l4 = new Location { Address = "Tokyo, Japan", Publisher = p4 };

                    GamesBD.Locations.AddRange(l1, l2, l3, l4);
                    GamesBD.SaveChanges();
                }
            }
        }

        static void Main(string[] args)
        {
            InitDB();

            while (true)
            {
                Console.Clear();
                Console.WriteLine(@"База видеоигр:
    1 - Все игры в базе
    2 - Добавить игру
    3 - Удалить игру
    4 - Общая статистика
    5 - Поиск
    0 - Выход из программы
");

                switch (char.ToLower(Console.ReadKey(true).KeyChar))
                {
                    case '1':
                        using (var db = new GameContext())
                        {
                            var games = db.VideoGames
                                .Include(m => m.Studio)
                                    .ThenInclude(ma => ma.Publisher)
                                    .ThenInclude(ma => ma.Location);

                            Console.Clear();

                            foreach (var i in games)
                            {
                                Console.WriteLine($"Title:\t\t{i.Name}\n" +
                                    $"Genre:\t\t{i.Genre}\t|\tRating: {i.Rating}\n" +
                                    $"Developer:\t{i.Studio.Name}\n" +
                                    $"Publisher:\t{i.Studio.Publisher.Name}\n" +
                                    $"Location:\t{i.Studio.Publisher.Location.Address}\n" +
                                    $"--------------------------------------------------");
                            }
                        }
                        Console.ReadKey();
                        break;

                    case '2':
                        Console.Clear();

                        Console.WriteLine("Введите название игры:");
                        string name = Console.ReadLine();
                        
                        Console.WriteLine("Введите жанр:");
                        string genre = Console.ReadLine();
                        
                        Console.WriteLine("Введите рэйтинг:");
                        float rating;
                        float.TryParse(Console.ReadLine(), out rating);

                        Console.WriteLine("Введите разработчика:");
                        string dev = Console.ReadLine();
                        
                        Console.WriteLine("Введите издателя:");
                        string publ = Console.ReadLine();
                        
                        Console.WriteLine("Введите адрес:");
                        string address = Console.ReadLine();

                        using (var db = new GameContext())
                        {
                            VideoGame v1 = new VideoGame { Name = name, Genre = genre, Rating = rating };
                            db.VideoGames.Add(v1);
                            db.SaveChanges();

                            Studio s1 = new Studio { Name = dev, VideoGame = v1 };
                            db.Studios.Add(s1);
                            db.SaveChanges();

                            Publisher p1 = new Publisher { Name = publ, Studios = new List<Studio>() { s1 } };

                            db.Publishers.Add(p1);
                            db.SaveChanges();

                            Location l1 = new Location { Address = address, Publisher = p1 };
                            db.Locations.Add(l1);
                            db.SaveChanges();
                        }

                        Console.Write("\nИгра добавлена!");

                        Console.ReadKey();
                        break;

                    case '3':
                        Console.Clear();

                        using (var db = new GameContext())
                        {
                            var games = db.VideoGames;

                            foreach (var i in games)
                            {
                                Console.WriteLine($"{i.ID} - {i.Name}");
                            }

                            Console.Write("Выберите игру для удаления (0 для выхода в меню): ");
                            int tmp;
                            int.TryParse(Console.ReadLine(), out tmp);

                            if(tmp == 0)
                            {
                                break;
                            }

                            var game = db.VideoGames.FirstOrDefault(v => v.ID == tmp);
                            if(game != null)
                            {
                                db.Remove(game);
                                db.SaveChanges();
                            }

                            Console.Write("\nИгра удалена!");

                            Console.ReadKey();
                        }
                        break;

                    case '4':
                        Console.Clear();

                        using (var db = new GameContext())
                        {
                            Console.WriteLine("Общая статистика:\n");

                            var totalGames = db.VideoGames.Count();
                            Console.WriteLine("Всего игр в базе:\t" + totalGames.ToString());

                            var totalStudios = db.Studios.Count();
                            Console.WriteLine("Всего студий в базе:\t" + totalStudios.ToString());

                            var totalPublishers = db.Publishers.Count();
                            Console.WriteLine("Всего издателей в базе:\t" + totalPublishers.ToString());

                            var topPublisher = db.Publishers.Include(a => a.Studios).OrderByDescending(a => a.Studios.Count).FirstOrDefault();
                            Console.WriteLine("\nИздатель с наибольшим колличеством студий:\t" + topPublisher.Name);

                            var topRated = db.VideoGames.OrderByDescending(a => a.Rating).FirstOrDefault();
                            Console.WriteLine("\nСамая популярная игра:\t\t" + topRated.Name);
                        }
                        Console.ReadKey();
                        break;

                    case '5':
                        Console.Clear();

                        Console.WriteLine(@"ПОИСК:

    1 - По жанру
    2 - По названию
    3 - По студии
    4 - По рейтингу
    0 - Выход в главное меню
");

                        switch (char.ToLower(Console.ReadKey(true).KeyChar))
                        {
                            case '1':
                                Console.WriteLine("Введите название жанра: ");
                                string g = Console.ReadLine().ToLower().Trim();
                                using (var db = new GameContext())
                                {
                                    var result = db.VideoGames.Where(a => a.Genre.ToLower().Trim() == g);
                                    if(result != null)
                                    {
                                        foreach(var i in result)
                                        {
                                            Console.WriteLine(i.Name);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Нет игры в данном жанре.");
                                    }

                                    Console.ReadKey();
                                }
                                break;

                            case '2':
                                Console.WriteLine("Введите ключевое слово из названия игры: ");
                                string v = Console.ReadLine().ToLower().Trim();
                                using (var db = new GameContext())
                                {
                                    var result = db.VideoGames.Where(a => a.Name.ToLower().Trim().Contains(v));
                                    if (result != null)
                                    {
                                        foreach (var i in result)
                                        {
                                            Console.WriteLine(i.Name);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Нет игр подходящих по словам.");
                                    }

                                    Console.ReadKey();
                                }
                                break;

                            case '3':
                                Console.WriteLine("Введите название студии или ключевые слова: ");
                                string s = Console.ReadLine().ToLower().Trim();
                                using (var db = new GameContext())
                                {
                                    var result = db.Studios.Where(a => a.Name.ToLower().Trim().Contains(s)).Include(a => a.VideoGame);
                                    if (result != null)
                                    {
                                        foreach (var i in result)
                                        {
                                            Console.WriteLine(i.VideoGame.Name);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Нет игр такой студии.");
                                    }

                                    Console.ReadKey();
                                }
                                break;

                            case '4':
                                Console.WriteLine("Введите интересующий рэйтинг: ");
                                float r;
                                float.TryParse(Console.ReadLine(), out r);
                                using (var db = new GameContext())
                                {
                                    var result = db.VideoGames.Where(a => a.Rating >= r);
                                    if (result != null)
                                    {
                                        foreach (var i in result)
                                        {
                                            Console.WriteLine(i.Name);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Нет игр выше указанного рэйтинга.");
                                    }

                                    Console.ReadKey();
                                }
                                break;

                            case '0':
                                return;

                            default:
                                break;
                        }

                        break;

                    case '0':
                        return;

                    default:
                        break;

                }
            }
        }
    }
}
