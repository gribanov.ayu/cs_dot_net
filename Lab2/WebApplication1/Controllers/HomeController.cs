﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly BandDBContext bandDB;

        public HomeController(BandDBContext logger)
        {
            bandDB = logger;

            if (!bandDB.Albums.Any())
            {
                Album a1 = new Album() { Name = "Solace", Year = 2006, Score = 10, Band = "Jakob" };
                Album a2 = new Album() { Name = "Sines", Year = 2016, Score = 9, Band = "Jakob" };
                Album a3 = new Album() { Name = "Hardcore will never die, but you will", Year = 2011, Score = 8, Band = "Mogwai" };
                Album a7 = new Album() { Name = "Come on, die young!", Year = 1999, Score = 6, Band = "Mogwai" };
                Album a8 = new Album() { Name = "The Hawk Is Howling", Year = 2008, Score = 10, Band = "Mogwai" };
                Album a4 = new Album() { Name = "Hymn to the immortal wind", Year = 2009, Score = 10, Band = "Mono" };
                Album a5 = new Album() { Name = "Dreams That Come a Thing", Year = 2008, Score = 7, Band = "Босх с тобой" };
                Album a6 = new Album() { Name = "Polaris", Year = 2008, Score = 7, Band = "EXXASENS" };
                Album a9 = new Album() { Name = "Tertia", Year = 2009, Score = 8, Band = "Caspian" };

                bandDB.Albums.AddRange(a1, a2, a3, a7, a8, a4, a5, a6, a9);
                bandDB.SaveChanges();
            }
        }

        public IActionResult Index()
        {
            return View(bandDB.Albums);
        }

        public IActionResult Delete(int? id)
        {
            var item = bandDB.Albums.FirstOrDefault(m => m.AlbumID == id);

            if (item == null)
                return NotFound();

            bandDB.Albums.Remove(item);
            bandDB.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int? id)
        {
            var item = bandDB.Albums.Where(a => a.AlbumID == id).FirstOrDefault();
            
            if (item == null)
                return NotFound();

            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Album album)
        {
            if (ModelState.IsValid)
            {
                bandDB.Entry(album).State = EntityState.Modified;
                bandDB.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(album);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Album album)
        {
            if (ModelState.IsValid)
            {
                bandDB.Entry(album).State = EntityState.Added;
                bandDB.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(album);
        }

        public IActionResult Statistics()
        {
            ViewBag.totalBands = bandDB.Albums.Select(a => a.Band).Distinct().Count();
            
            ViewBag.topAlbum = bandDB.Albums.OrderByDescending(a => a.Score).FirstOrDefault().Name;
            
            ViewBag.worstAlbum = bandDB.Albums.OrderBy(a => a.Score).FirstOrDefault().Name;
            
            ViewBag.latestRelease = bandDB.Albums.OrderByDescending(a => a.Year).FirstOrDefault().Name;

            ViewBag.totalAlbums = bandDB.Albums.Count();

            ViewBag.mostAlbums = bandDB.Albums.ToList()
                .GroupBy(x => x.Band)
                .Where(g => g.Count() > 1)
                .ToDictionary(x => x.Key, y => y.Count())
                .OrderByDescending(x => x.Value)
                .FirstOrDefault().Key;

            return View();
        }

        public IActionResult Search(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var str = text.ToLower().Trim();

                return View("Index", bandDB.Albums
                    .Where(a => 
                    a.Name.ToLower().Contains(str) || 
                    a.Band.ToLower().Contains(str) || 
                    a.Score.ToString().Contains(str) || 
                    a.Year.ToString().Contains(str)));
            }

            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
