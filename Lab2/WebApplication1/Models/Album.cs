﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Album
    {
        [Key]
        public int AlbumID { get; set; }

        [Required(ErrorMessage = "Введите название!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите год выпуска!")]
        [Range(1700, 2020, ErrorMessage = "Укажите верный год!")]
        public int? Year { get; set; }

        [Required(ErrorMessage = "Введите оценку!")]
        [Range(1, 10, ErrorMessage = "Значение от 1 - 10!")]
        public int? Score { get; set; }

        [Required(ErrorMessage = "Введите группу!")]
        public string Band { get; set; }
    }
}
