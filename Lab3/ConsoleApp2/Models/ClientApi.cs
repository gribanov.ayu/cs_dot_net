﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Models
{
    class ClientApi
    {
        private string gamesURL = @"http://localhost:53817/api/Games";
        private string studiosURL = @"http://localhost:53817/api/Studios";
        private string publishersURL = @"http://localhost:53817/api/Publishers";

        HttpClient client;
        public ClientApi()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Game>> GetAllGameAsync()
        {
            List<Game> all = null;

            HttpResponseMessage response = await client.GetAsync(gamesURL);

            if (response.IsSuccessStatusCode)
            {
                all = await response.Content.ReadAsAsync<List<Game>>();
            }

            return all;
        }

        public async Task<List<Studio>> GetAllStudioAsync()
        {
            List<Studio> all = null;

            HttpResponseMessage response = await client.GetAsync(studiosURL);

            if (response.IsSuccessStatusCode)
            {
                all = await response.Content.ReadAsAsync<List<Studio>>();
            }

            return all;
        }

        public async Task<List<Publisher>> GetAllPublisherAsync()
        {
            List<Publisher> all = null;

            HttpResponseMessage response = await client.GetAsync(publishersURL);

            if (response.IsSuccessStatusCode)
            {
                all = await response.Content.ReadAsAsync<List<Publisher>>();
            }

            return all;
        }

        public async Task<Uri> CreateGameAsync(Game g)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(gamesURL, g);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        public async Task<Uri> CreateStudioAsync(Studio s)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(studiosURL, s);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        public async Task<Uri> CreatePublisherAsync(Publisher p)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(publishersURL, p);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        public async Task<HttpStatusCode> DeleteGameAsync(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync(gamesURL + "/" + id);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> DeleteStudioAsync(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync(studiosURL + "/" + id);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> DeletePublisherAsync(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync(publishersURL + "/" + id);
            return response.StatusCode;
        }

        public async Task<Game> GetGameAsync(int id)
        {
            Game g = null;

            HttpResponseMessage response = await client.GetAsync(gamesURL + "/" + id);

            if (response.IsSuccessStatusCode)
            {
                g = await response.Content.ReadAsAsync<Game>();
            }

            return g;
        }

        public async Task<Studio> GetStudioAsync(int id)
        {
            Studio s = null;

            HttpResponseMessage response = await client.GetAsync(studiosURL + "/" + id);

            if (response.IsSuccessStatusCode)
            {
                s = await response.Content.ReadAsAsync<Studio>();
            }

            return s;
        }

        public async Task<Publisher> GetPublisherAsync(int id)
        {
            Publisher p = null;

            HttpResponseMessage response = await client.GetAsync(publishersURL + "/" + id);

            if (response.IsSuccessStatusCode)
            {
                p = await response.Content.ReadAsAsync<Publisher>();
            }

            return p;
        }

        public async Task<HttpStatusCode> PutGameAsync(Game g)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(gamesURL + "/" + g.GameID.ToString(), g);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> PutStudioAsync(Studio s)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(studiosURL + "/" + s.StudioID.ToString(), s);
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> PutPublisherAsync(Publisher p)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(publishersURL + "/" + p.PublisherID.ToString(), p);
            return response.StatusCode;
        }
    }
}
