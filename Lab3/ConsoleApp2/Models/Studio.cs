﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp2.Models
{
    public class Studio
    {
        public int StudioID { get; set; }
        public string Name { get; set; }
        public int PublisherID { get; set; }
        public ICollection<Game> Games { get; set; }
        public Publisher Publisher { get; set; }
    }
}
