﻿using ConsoleApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static ClientApi client = new ClientApi();

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(@"База видеоигр:
    1 - Все игры в базе
    2 - Добавить игру
    3 - Удалить игру/студию/издателя
    4 - Редактирование
    0 - Выход из программы
");

                switch (char.ToLower(Console.ReadKey(true).KeyChar))
                {
                    case '1':
                        GetInDataBase().GetAwaiter().GetResult();
                        break;

                    case '2':
                        PostInDataBase().GetAwaiter().GetResult();
                        break;

                    case '3':
                        DeleteInDataBase().GetAwaiter().GetResult();
                        break;

                    case '4':
                        PutInDataBase().GetAwaiter().GetResult();
                        break;

                    case '0':
                        return;

                    default:
                        break;
                }
            }
        }

        static async Task PutInDataBase()
        {
            Console.Clear();

            Console.WriteLine(@"База видеоигр:
    1 - Редактировать игру
    2 - Редактировать студию
    3 - Редактировать издателя
");
            int id;
            string newName;
            HttpStatusCode statusCode;

            switch (char.ToLower(Console.ReadKey(true).KeyChar))
            {
                case '1':
                    Console.Clear();
                    Console.WriteLine("Выберите игру по индексу для редактирования:");

                    var games = await client.GetAllGameAsync();

                    foreach (var i in games)
                    {
                        Console.WriteLine(i.GameID.ToString() + " -> Название: " + i.Name + "\n\tОценка: " + i.Rating);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    Game g = await client.GetGameAsync(id);

                    if (g == null)
                    {
                        Console.WriteLine("Нет такой игры!");
                        Console.ReadKey();
                        break;
                    }

                    Console.WriteLine($"Title:\t\t{g.Name}\n" +
                                    $"Genre:\t\t{g.Genre}\nRating:\t\t{g.Rating}\nYear:\t\t{g.Year}\n" +
                                    $"--------------------------------------------------");

                    Console.WriteLine($"Новое название для {g.Name}:\n");
                    newName = Console.ReadLine();

                    if(newName == "")
                    {
                        Console.WriteLine("Не переименован!");
                        Console.ReadKey();
                        break;
                    }

                    Console.WriteLine($"Новый рейтинг для {g.Name}:\n");
                    float.TryParse(Console.ReadLine(), out float rating);

                    g.Name = newName;
                    g.Rating = rating;

                    statusCode = await client.PutGameAsync(g);

                    if (statusCode.ToString() == "NoContent")
                    {
                        Console.WriteLine("Редактирование завершено [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Не удалось отредактировать [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine("Выберите студию по индексу для редактирования:");

                    var studios = await client.GetAllStudioAsync();

                    foreach (var i in studios)
                    {
                        Console.WriteLine(i.StudioID.ToString() + " -> Название: " + i.Name);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    Studio s = await client.GetStudioAsync(id);

                    if (s == null)
                    {
                        Console.WriteLine("Нет такой студии!");
                        Console.ReadKey();
                        break;
                    }

                    Console.WriteLine($"Новое название для {s.Name}:\n");
                    newName = Console.ReadLine();

                    if (newName == "")
                    {
                        Console.WriteLine("Не переименован!");
                        Console.ReadKey();
                        break;
                    }

                    s.Name = newName;

                    statusCode = await client.PutStudioAsync(s);

                    if (statusCode.ToString() == "NoContent")
                    {
                        Console.WriteLine("Студия переименована [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Студия не переименована [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                case '3':
                    Console.Clear();
                    Console.WriteLine("Выберите издателя по индексу для редактирования:");

                    var publishers = await client.GetAllPublisherAsync();

                    foreach (var i in publishers)
                    {
                        Console.WriteLine(i.PublisherID.ToString() + " -> Название: " + i.Name);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    Publisher p = await client.GetPublisherAsync(id);

                    if(p == null)
                    {
                        Console.WriteLine("Нет такого издателя!");
                        Console.ReadKey();
                        break;
                    }

                    Console.WriteLine($"Новое название для {p.Name}:\n");
                    newName = Console.ReadLine();

                    if (newName == "")
                    {
                        Console.WriteLine("Не переименован!");
                        Console.ReadKey();
                        break;
                    }

                    p.Name = newName;

                    statusCode = await client.PutPublisherAsync(p);

                    if (statusCode.ToString() == "NoContent")
                    {
                        Console.WriteLine("Издатель переименован [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Издатель не переименован [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                case '0':
                    return;

                default:
                    break;
            }
        }

        static async Task DeleteInDataBase()
        {
            Console.Clear();

            Console.WriteLine(@"База видеоигр:
    1 - Удалить игру
    2 - Удалить студию
    3 - Удалить издателя
");
            int id;
            HttpStatusCode statusCode;

            switch (char.ToLower(Console.ReadKey(true).KeyChar))
            {
                case '1':
                    Console.Clear();
                    Console.WriteLine("Выберите игру по индексу для удаления:");

                    var games = await client.GetAllGameAsync();

                    foreach (var i in games)
                    {
                        Console.WriteLine(i.GameID.ToString() + " -> Название: " + i.Name);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    statusCode = await client.DeleteGameAsync(id);

                    if(statusCode.ToString() == "OK")
                    {
                        Console.WriteLine("Игра удалена [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Игра не удалена [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                case '2':
                    Console.Clear();
                    Console.WriteLine("Выберите студию по индексу для удаления:");

                    var studios = await client.GetAllStudioAsync();

                    foreach (var i in studios)
                    {
                        Console.WriteLine(i.StudioID.ToString() + " -> Название: " + i.Name);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    statusCode = await client.DeleteStudioAsync(id);

                    if (statusCode.ToString() == "OK")
                    {
                        Console.WriteLine("Студия удалена [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Студия не удалена [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                case '3':
                    Console.Clear();
                    Console.WriteLine("Выберите издателя по индексу для удаления:");

                    var publishers = await client.GetAllPublisherAsync();

                    foreach (var i in publishers)
                    {
                        Console.WriteLine(i.PublisherID.ToString() + " -> Название: " + i.Name);
                    }
                    int.TryParse(Console.ReadLine(), out id);

                    statusCode = await client.DeletePublisherAsync(id);

                    if (statusCode.ToString() == "OK")
                    {
                        Console.WriteLine("Издатель удален [Status Code]:[" + statusCode.ToString() + "]");
                    }
                    else
                    {
                        Console.WriteLine("Издатель не удален [Status Code]:[" + statusCode.ToString() + "]");
                    }

                    Console.ReadKey();
                    break;

                default:
                    break;
            }
        }
        
        static async Task PostInDataBase()
        {
            Console.Clear();
            Console.WriteLine("База видеоигр:\n");

            Console.Write("Введите название игры: ");
            string name = Console.ReadLine();

            Console.Write("Введите жанр: ");
            string genre = Console.ReadLine();

            Console.Write("Введите рэйтинг (формат: 7,75): ");
            float.TryParse(Console.ReadLine(), out float rating);

            Console.Write("Введите год создания: ");
            int.TryParse(Console.ReadLine(), out int year);

            Console.Write("Введите студию: ");
            string studio = Console.ReadLine();

            if(name == "" || genre == "" || year == 0 || studio == "")
            {
                Console.WriteLine("Не указано одно из полей, повторите еще!");
                Console.ReadKey();
                return;
            }

            var studios = await client.GetAllStudioAsync();
            bool addStudio = true;

            // проверка есть ли такая студия в базе
            foreach (var i in studios)
            {
                if (studio.ToLower().Trim() == i.Name.ToLower().Trim())
                {
                    addStudio = false;
                    break;
                }
            }

            // если нет, создаем и указываем издателя
            if(addStudio)
            {
                Console.Write("Введите издателя: ");
                string publisher = Console.ReadLine();

                if (publisher == "")
                {
                    Console.WriteLine("Издатель не указан!");
                    Console.ReadKey();
                    return;
                }

                var publishers = await client.GetAllPublisherAsync();
                bool addPublisher = true;

                foreach (var i in publishers)
                {
                    if (publisher.ToLower().Trim() == i.Name.ToLower().Trim())
                    {
                        addPublisher = false;
                        break;
                    }
                }

                // если нет такого издателя в базе - создаем нового
                if (addPublisher)
                {
                    Publisher p = new Publisher() { Name = publisher };

                    await client.CreatePublisherAsync(p);
                }

                if (addStudio)
                {
                    publishers = await client.GetAllPublisherAsync();

                    Studio s = new Studio()
                    {
                        Name = studio,
                        PublisherID = publishers.Where(a => a.Name.ToLower().Trim() == publisher.ToLower().Trim()).Select(a => a.PublisherID).FirstOrDefault()
                    };

                    await client.CreateStudioAsync(s);
                }
            }

            studios = await client.GetAllStudioAsync();
            Game g = new Game() {
                Name = name,
                Genre = genre,
                Rating = rating,
                Year = year,
                StudioID = studios.Where(a => a.Name.ToLower().Trim() == studio.ToLower().Trim()).Select(a => a.StudioID).FirstOrDefault()
            };

            await client.CreateGameAsync(g);

            Console.WriteLine("Игра добавлена в базу.");
            Console.ReadKey();
        }

        static async Task GetInDataBase()
        {
            Console.Clear();
            var games = await client.GetAllGameAsync();
            var studios = await client.GetAllStudioAsync();
            var publishers = await client.GetAllPublisherAsync();

            Console.WriteLine($"Все игры в базе:\n" +
                $"--------------------------------------------------");

            var gameStudio = from g in games
                             join s in studios
                             on g.StudioID equals s.StudioID
                             select new
                             {
                                 game = g.Name,
                                 year = g.Year,
                                 studio = s.Name,
                                 score = g.Rating,
                                 genre = g.Genre,
                                 publisher_id = s.PublisherID
                             };

            var gameStudioPublisher = from g in gameStudio.ToList()
                                      join p in publishers
                                      on g.publisher_id equals p.PublisherID
                                      select new
                                      {
                                          g.game,
                                          g.genre,
                                          g.year,
                                          g.studio,
                                          g.score,
                                          publisher = p.Name
                                      };

            foreach (var i in gameStudioPublisher.ToHashSet())
            {
                Console.WriteLine($"Title:\t\t{i.game}\n" +
                                    $"Genre:\t\t{i.genre}\nRating:\t\t{i.score}\nYear:\t\t{i.year}\n" +
                                    $"Developer:\t{i.studio}\n" +
                                    $"Publisher:\t{i.publisher}\n" +
                                    $"--------------------------------------------------");
            }

            Console.ReadKey();
        }
    }
}
