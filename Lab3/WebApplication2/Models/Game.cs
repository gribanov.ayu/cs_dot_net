﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Game
    {
        [Key]
        public int GameID { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public float Rating { get; set; }
        public int StudioID { get; set; }
        public Studio Studio { get; set; }
    }
}
