﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Models;

namespace WebApplication2.Models
{
    public class GameContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Studio> Studios { get; set; }
        public DbSet<Publisher> Publisher { get; set; }
        public GameContext()
        {
            // Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=MyDataBase;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>()
                .HasOne(s => s.Studio)
                .WithMany(g => g.Games)
                .HasForeignKey(s => s.StudioID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Studio>()
                .HasOne(a => a.Publisher)
                .WithMany(a => a.Studios)
                .HasForeignKey(a => a.PublisherID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Publisher>().HasData(
                new Publisher
                {
                    Name = "Ubisoft",
                    PublisherID = 1
                },
                new Publisher
                {
                    Name = "Rockstar Games",
                    PublisherID = 2
                }
            );

            modelBuilder.Entity<Studio>().HasData(
                new Studio
                {
                    Name = "Ubisoft Montreal",
                    StudioID = 1,
                    PublisherID = 1
                },
                new Studio
                {
                    Name = "Ubisoft Montpellier",
                    StudioID = 2,
                    PublisherID = 1
                },
                new Studio
                {
                    Name = "Rockstar North",
                    StudioID = 3,
                    PublisherID = 2
                },
                new Studio
                {
                    Name = "Rockstar San Diego",
                    StudioID = 4,
                    PublisherID = 2
                }
            );

            modelBuilder.Entity<Game> ().HasData(
                new Game
                {
                    Name = "Prince of Persia: The Sands of Time",
                    Genre = "RPG",
                    Rating = 8.7F,
                    Year = 2003,
                    GameID = 1,
                    StudioID = 1
                },
                new Game
                {
                    Name = "Beyond Good & Evil",
                    Genre = "RPG",
                    Rating = 7.68F,
                    Year = 2003,
                    GameID = 2,
                    StudioID = 2
                },
                new Game
                {
                    Name = "Grand Theft Auto III",
                    Genre = "Action-adventure",
                    Rating = 9.2F,
                    Year = 2001,
                    GameID = 3,
                    StudioID = 3
                },
                new Game
                {
                    Name = "Red Dead Redemption",
                    Genre = "Action-adventure",
                    Rating = 9.35F,
                    Year = 2010,
                    GameID = 4,
                    StudioID = 4
                },
                new Game
                {
                    Name = "Max Payne 3",
                    Genre = "Third-person shooter",
                    Rating = 8.35F,
                    Year = 2012,
                    GameID = 5,
                    StudioID = 3
                },
                new Game
                {
                    Name = "Assassin's Creed",
                    Genre = "Action-adventure",
                    Rating = 8.15F,
                    Year = 2007,
                    GameID = 6,
                    StudioID = 1
                }
            );
        }
    }
}
