﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Publisher
    {
        [Key]
        public int PublisherID { get; set; }
        public string Name { get; set; }
        public ICollection<Studio> Studios { get; set; }
    }
}
